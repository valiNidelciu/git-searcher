(function () {
    'use strict';

    angular
        .module('vapp.gitSearch')
        .factory('github', github);

    github.$inject = ['$http'];
    function github($http) {
        return {
            getUser: getUser,
            getRepos: getRepos

        };
        function getUser(username) {
            return $http.get("https://api.github.com/users/" + username).then(function (response) {
                return response.data;
            });

        };
        function getRepos(username){
            return $http.get("https://api.github.com/users/" + username +"/repos").then(function(response){
                return response.data;
            });
        }

    }

})();
