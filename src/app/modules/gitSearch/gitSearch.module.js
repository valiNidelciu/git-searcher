(function() {
    'use strict';

    angular.module('vapp.gitSearch', [])
           .config(config);
       function config($stateProvider){
           $stateProvider 
                .state("gitSearch",{
                    url:"/gitSearch",
                    templateUrl:"app/modules/gitSearch/gitSearch.html",
                    controller:"gitSearchController as vm"
                });
       }  
})();

