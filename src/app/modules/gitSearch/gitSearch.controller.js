(function () {
    'use strict';

    angular
        .module('vapp.gitSearch')
        .controller('gitSearchController', gitSearchController);

    gitSearchController.$inject = ['github', '$scope'];
    function gitSearchController(github, $scope) {
        var vm = this;

        function userReqComplete(data) {
            $scope.user = data;
            console.log("ok,user,getting repos");
            
        };
        function reposReqComplete(data){
            $scope.repos=data;
        }
    
        

        function onError() {
            alert("This user can't be found")
        };


        $scope.submit = function (username) {
            vm.submitted=true;
            console.log("ok,submit")
            github.getUser(username).then(userReqComplete, onError);
            github.getRepos(username).then(reposReqComplete, onError);



        }

    }
})();



