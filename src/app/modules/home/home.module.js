(function() {
    'use strict';

    angular.module('vapp.home', [])
           .config(config);

      function config($stateProvider, $urlRouterProvider){
            $stateProvider
                .state("home",{
                    url:"/home",
                    templateUrl:"app/modules/home/home.html",
                    controller: "HomeController as vm"
                    
                })
            $urlRouterProvider.otherwise("/home")    
        } 
})();